from rest_framework import serializers
from .models import *

class CourierSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model : Curier
        fields: ('name','phone_number')

class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model : Customer
        fields: ('name','phone_number','hash','invoicing_details')

class InvoiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model : Invoice
        fields: ('company_name','company_address','tax_id')

class AttributeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model : Attribute
        fields: ('id','quantity','name','price')

class DeliverySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model : Delivery
        fields: ('label','longitude','latitude')

class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model : Product
        fields: ('id','quantity','name','price','purchased_product_id')

class OrderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model : Order
        fields: ('order_id','store_id','order_time','estimated_pickup_time',
                 'payment_method','currency','order_code','allergy_info',
                 'estimated_total_price','delivery_fee','minimum_basket_surcharge',
                 'customer_cash_payment_amount','courier','customer','delivery_address',
                 'pick_up_code','is_picked_up_by_customer','cutlery_requested','total_customer_to_pay')