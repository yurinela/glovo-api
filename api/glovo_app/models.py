from django.db import models

class Courier(models.Model):
    name = models.CharField(max_length = 64)
    phone_number = models.CharField(max_length = 64)

class Customer(models.Model):
    name = models.CharField(max_length = 64)
    phone_number = models.CharField(max_length = 64)
    hash = models.CharField(max_length = 64)
    invoicing_details = models.ForeignKey('Invoice', on_delete=models.CASCADE)

class Invoice(models.Model): 
    company_name = models.CharField(max_length = 64)
    company_address = models.CharField(max_length = 64)
    tax_id = models.CharField(max_length = 64)

class Attribute(models.Model):
    id = models.CharField(max_length = 64, primary_key=True)
    quantity = models.IntegerField()
    name = models.CharField(max_length = 64)
    price = models.PositiveIntegerField()

class Delivery(models.Model): #factura-entrega
    label = models.CharField(max_length = 64)
    longitude = models.CharField(max_length = 64)
    latitude = models.CharField(max_length = 64)

class Product(models.Model):
    id = models.CharField(max_length = 64, primary_key=True)
    quantity = models.PositiveIntegerField()
    name = models.CharField(max_length = 64)
    price = models.PositiveIntegerField()    
    purchased_product_id = models.CharField(max_length = 64)

class Order(models.Model):
    order_id = models.CharField(max_length = 64,primary_key=True)
    store_id = models.CharField(max_length = 64)
    order_time = models.CharField(max_length = 64)  # yyyy-MM-dd HH:mm:ss
    estimated_pickup_time = models.CharField(max_length = 64)
    payment_method = models.CharField(max_length = 64)
    currency = models.CharField(max_length = 64)
    order_code = models.CharField(max_length = 64)
    allergy_info = models.CharField(max_length = 64)
    estimated_total_price = models.PositiveIntegerField()
    delivery_fee = models.PositiveIntegerField()
    minimum_basket_surcharge = models.PositiveIntegerField()
    customer_cash_payment_amount = models.PositiveIntegerField()
    courier = models.ForeignKey('Courier', on_delete=models.CASCADE)
    customer = models.ForeignKey('Customer', on_delete=models.CASCADE)
    # products = 
    delivery_address = models.ForeignKey('Delivery', on_delete=models.CASCADE)
    # bundled_orders = 
    pick_up_code =  models.CharField(max_length = 64)
    is_picked_up_by_customer = models.BooleanField()
    cutlery_requested = models.BooleanField()
    total_customer_to_pay = models.DecimalField(max_digits=10, decimal_places = 2)

class OrderCancelation(models.Model):
    CANCEL_REASON = (
        ('PRODUCTS_NOT_AVAILABLE', 'Product not available'),
        ('STORE_CAN_NOT_DELIVER', 'Can\'t not delivery'),
        ('PARTNER_PRINTER_ISSUE', 'Partner printer issue'),
        ('USER_ERROR', 'User error'),
        ('ORDER_NOT_FEASIBLE', 'Order not feasible'),
        ('OTHER', 'Other'),
    )

    PAYMENT_STRATEGY = (
        ('PAY_NOTHING', 'Pay nothing'),
        ('PAY_PRODUCTS', 'Pay products'),        
    )

    order_id = models.ChartField(max_length = 64)
    cancel_reason = models.ChartField(max_length = 22, choices=CANCEL_REASON)
    payment_strategy = models.ChartField(max_length = 12, choices=PAYMENT_STRATEGY)

class Menu(models.Model):
    menuUrl = models.ChartField(max_length = 255)


class AttributeMenu(models.Model):
    id = models.CharField(max_length = 64,primary_key=True)
    name = models.CharField(max_length = 64)
    price_impact = models.CharField(max_length = 64)
    selected_by_default = models.CharField(max_length = 5, choices = ["true", "false"])
    available = models.BooleanField(default=True)

class AttributeGroupMenu(models.Model):
    id = models.CharField(max_length = 64,primary_key=True)
    name = models.CharField(max_length = 64)
    min = models.CharField(max_length = 64)
    max = models.CharField(max_length = 64)
    multiple_selection = models.CharField(max_length = 5, choices = ["true", "false"])
    attributesattributes = []

class ProductMenu(models.Model):
    id = models.CharField(max_length = 64,primary_key=True)
    name = models.CharField(max_length = 64)
    price = models.CharField(max_length = 64)
    image_url = models.CharField(max_length = 255)
    description = models.CharField(max_length = 1024)
    attributes_groups = []
    available = models.BooleanField(default=True, required=False)
