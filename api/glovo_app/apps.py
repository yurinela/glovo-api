from django.apps import AppConfig


class GlovoAppConfig(AppConfig):
    name = 'glovo_app'
