from django.shortcuts import render
from rest_framework import viewsets
from .models import Order

class OrderAddViewSet(viewsets.ModelViewSet):
    """
    Endpoint that allow add an order.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

