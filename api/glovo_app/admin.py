from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Courier)
admin.site.register(Customer)
admin.site.register(Invoice)
admin.site.register(Attribute)
admin.site.register(Delivery)
admin.site.register(Product)
admin.site.register(Order)

